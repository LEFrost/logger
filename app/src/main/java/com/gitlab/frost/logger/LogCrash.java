package com.gitlab.frost.logger;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;

public class LogCrash implements Thread.UncaughtExceptionHandler {
    private Thread.UncaughtExceptionHandler mDefaultHandler;

    public LogCrash() {
        mDefaultHandler= Thread.getDefaultUncaughtExceptionHandler();
    }

    @Override
    public void uncaughtException(Thread thread, Throwable throwable) {
        //获取崩溃信息
        String stackTraceInfo = getStackTraceInfo(throwable);
        //将崩溃信息记录
        saveThrowableMessage(stackTraceInfo);
        //给默认处理器处理
        mDefaultHandler.uncaughtException(thread,throwable);
    }

    /**
     * 获取错误的信息
     */
    private String getStackTraceInfo(final Throwable throwable) {
        PrintWriter pw = null;
        Writer writer = new StringWriter();
        try {
            pw = new PrintWriter(writer);
            throwable.printStackTrace(pw);
        } catch (Exception e) {
            return "";
        } finally {
            if (pw != null) {
                pw.close();
            }
        }
        return writer.toString();
    }


    /**
     * 记录奔溃信息到log文件
     */

    private void saveThrowableMessage(String errorMessage) {
        Logger.error(errorMessage);
    }


}

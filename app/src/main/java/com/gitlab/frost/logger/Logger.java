package com.gitlab.frost.logger;

import org.slf4j.Marker;

public class Logger {
    private static org.slf4j.Logger mLog;

    public static void init(org.slf4j.Logger log) {
        mLog = log;
        Thread.setDefaultUncaughtExceptionHandler(new LogCrash());
    }

    private static void checkInitLog() {
        if (mLog == null) {
            throw new NullPointerException("使用前必须初始化log");
        }
    }


    public static void trace(String msg) {
        mLog.trace(msg);
    }

    public static void trace(String format, Object arg) {
        mLog.trace(format, arg);
    }

    public static void trace(String format, Object arg1, Object arg2) {
        mLog.trace(format, arg1, arg2);
    }

    public static void trace(String format, Object... arguments) {
        mLog.trace(format, arguments);
    }

    public static void trace(String msg, Throwable t) {
        mLog.trace(msg, t);
    }


    public static void trace(Marker marker, String msg) {
        mLog.trace(marker, msg);
    }

    public static void trace(Marker marker, String format, Object arg) {
        mLog.trace(marker, format, arg);
    }

    public static void trace(Marker marker, String format, Object arg1, Object arg2) {
        mLog.trace(marker, format, arg1, arg2);
    }

    public static void trace(Marker marker, String format, Object... argArray) {
        mLog.trace(marker, format, argArray);
    }

    public static void trace(Marker marker, String msg, Throwable t) {
        mLog.trace(marker, msg, t);
    }

    public static void debug(String msg) {
        mLog.debug(msg);
    }

    public static void debug(String format, Object arg) {
        mLog.debug(format, arg);
    }

    public static void debug(String format, Object arg1, Object arg2) {
        mLog.debug(format, arg1, arg2);
    }

    public static void debug(String format, Object... arguments) {
        mLog.debug(format, arguments);
    }

    public static void debug(String msg, Throwable t) {
        mLog.debug(msg, t);
    }

    public static void debug(Marker marker, String msg) {
        mLog.debug(marker, msg);
    }

    public static void debug(Marker marker, String format, Object arg) {
        mLog.debug(marker, format, arg);
    }

    public static void debug(Marker marker, String format, Object arg1, Object arg2) {
        mLog.debug(marker, format, arg1, arg2);
    }

    public static void debug(Marker marker, String format, Object... arguments) {
        mLog.debug(marker, format, arguments);
    }

    public static void debug(Marker marker, String msg, Throwable t) {
        mLog.debug(marker, msg, t);
    }


    public static void info(String msg) {
        mLog.info(msg);
    }

    public static void info(String format, Object arg) {
        mLog.info(format, arg);
    }

    public static void info(String format, Object arg1, Object arg2) {
        mLog.info(format, arg1, arg2);
    }

    public static void info(String format, Object... arguments) {
        mLog.info(format, arguments);
    }

    public static void info(String msg, Throwable t) {
        mLog.info(msg, t);
    }


    public static void info(Marker marker, String msg) {
        mLog.info(marker, msg);
    }

    public static void info(Marker marker, String format, Object arg) {
        mLog.info(marker, format, arg);
    }

    public static void info(Marker marker, String format, Object arg1, Object arg2) {
        mLog.info(marker, format, arg1, arg2);
    }

    public static void info(Marker marker, String format, Object... arguments) {
        mLog.info(marker, format, arguments);
    }

    public static void info(Marker marker, String msg, Throwable t) {
        mLog.info(marker, msg, t);
    }

    public static void warn(String msg) {
        mLog.warn(msg);
    }

    public static void warn(String format, Object arg) {
        mLog.warn(format, arg);
    }

    public static void warn(String format, Object arg1, Object arg2) {
        mLog.warn(format, arg1, arg2);
    }

    public static void warn(String format, Object... arguments) {
        mLog.warn(format, arguments);
    }

    public static void warn(String msg, Throwable t) {
        mLog.warn(msg, t);
    }


    public static void warn(Marker marker, String msg) {
        mLog.warn(marker, msg);
    }

    public static void warn(Marker marker, String format, Object arg) {
        mLog.warn(marker, format, arg);
    }

    public static void warn(Marker marker, String format, Object arg1, Object arg2) {
        mLog.warn(marker, format, arg1, arg2);
    }

    public static void warn(Marker marker, String format, Object... arguments) {
        mLog.warn(marker, format, arguments);
    }

    public static void warn(Marker marker, String msg, Throwable t) {
        mLog.warn(marker, msg, t);
    }

    public static void error(String msg) {
        mLog.error(msg);
    }

    public static void error(String format, Object arg) {
        mLog.error(format, arg);
    }

    public static void error(String format, Object arg1, Object arg2) {
        mLog.error(format, arg1, arg2);
    }

    public static void error(String format, Object... arguments) {
        mLog.error(format, arguments);
    }

    public static void error(String msg, Throwable t) {
        mLog.error(msg, t);
    }


    public static void error(Marker marker, String msg) {
        mLog.error(marker, msg);
    }

    public static void error(Marker marker, String format, Object arg) {
        mLog.error(marker, format, arg);
    }

    public static void error(Marker marker, String format, Object arg1, Object arg2) {
        mLog.error(marker, format, arg1, arg2);
    }

    public static void error(Marker marker, String format, Object... arguments) {
        mLog.error(marker, format, arguments);
    }

    public static void error(Marker marker, String msg, Throwable t) {
        mLog.error(marker, msg, t);
    }
}
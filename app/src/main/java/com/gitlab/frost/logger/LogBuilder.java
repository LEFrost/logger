package com.gitlab.frost.logger;

import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.ArrayList;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.android.LogcatAppender;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.FileAppender;
import ch.qos.logback.core.UnsynchronizedAppenderBase;
import ch.qos.logback.core.rolling.RollingFileAppender;
import ch.qos.logback.core.rolling.SizeAndTimeBasedFNATP;
import ch.qos.logback.core.rolling.TimeBasedRollingPolicy;
import ch.qos.logback.core.util.FileSize;


public class LogBuilder {
    public static class LOG_MODE {
        public static int LOGCAT = 1;
        public static int FILE_ONLY_ONE = 1 << 1;
        public static int FILE_BY_Day = 1 << 2;
    }

    public LogBuilder setLogFormat(String mLogFormat) {
        this.mLogFormat = mLogFormat;
        return this;
    }

    public LogBuilder setLogPath(String mLogPath) {
        this.mLogPath = mLogPath;
        return this;

    }

    public LogBuilder setFilename(String mFilename) {
        this.mFilename = mFilename;
        return this;

    }

    public LogBuilder setMode(int mMode) {
        this.mMode = mMode;
        return this;

    }

    public LogBuilder setTag(String mTag) {
        this.mTag = mTag;
        return this;

    }

    /**
     * @Description: 设置单个文件大小
     * @Param: [mSize]
     * @return: com.cigit.airportwholeprocess.utils.LogBuilder
     * @Author: PengJian
     * @Date: 2019/7/24
     */
    public LogBuilder setSize(long mSize) {
        this.mSize = mSize;
        return this;

    }

    public LogBuilder setDateFormat(String format) {
        mDateFormat = format;
        return this;
    }

    private String mDateFormat = "yyyy-MM-dd";
    private String mLogFormat = "[%thread] %-5level %logger{36} - %msg%n";
    private String mLogPath = "";
    private String mFilename = "log";
    private int mMode = 1;
    private String mTag = "CIGIT";
    //日志大小,默认1M
    private long mSize = 1024 * 1024L;
    private LoggerContext mContext = (LoggerContext) LoggerFactory.getILoggerFactory();

    private ArrayList<UnsynchronizedAppenderBase<ILoggingEvent>> mAppenders = new ArrayList<UnsynchronizedAppenderBase<ILoggingEvent>>();

    public Logger build() {

        mContext.stop();
        if ((LOG_MODE.FILE_BY_Day & mMode) != 0) {
            addFileAppenderByDate();
        }
        if ((LOG_MODE.FILE_ONLY_ONE & mMode) != 0) {
            addFileAppender();
        }
        if ((LOG_MODE.LOGCAT & mMode) != 0) {
            addLogAppender();
        }

        ch.qos.logback.classic.Logger logger = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(mTag);
        addAppender(logger);
        return logger;
    }

    private void checkSetPath() {
        if (mLogPath.isEmpty()) {
            throw new IllegalArgumentException("使用文件存储时必须指定文件路径");
        }
    }

    private void addFileAppenderByDate() {
        checkSetPath();
        RollingFileAppender<ILoggingEvent> appender = new RollingFileAppender<ILoggingEvent>();
        appender.setAppend(true);
        appender.setContext(mContext);
        //设置时间滚动
        TimeBasedRollingPolicy<ILoggingEvent> rollingPolicy = new TimeBasedRollingPolicy<ILoggingEvent>();
        //%i时必须添加项，用于区分滚动文档,超过文件限定大小将累加i

        rollingPolicy.setFileNamePattern(mLogPath + File.separatorChar + mFilename + "_%d{" + mDateFormat + "}_%i.log");
        //保留天数
        rollingPolicy.setMaxHistory(7);
        rollingPolicy.setParent(appender);
        rollingPolicy.setContext(mContext);

        //限制一个log的大小
        SizeAndTimeBasedFNATP<ILoggingEvent> sizeAndTimeBasedFNATP = new SizeAndTimeBasedFNATP<ILoggingEvent>();
        sizeAndTimeBasedFNATP.setMaxFileSize(new FileSize(mSize));
        sizeAndTimeBasedFNATP.setContext(mContext);
        sizeAndTimeBasedFNATP.setTimeBasedRollingPolicy(rollingPolicy);

        rollingPolicy.setTimeBasedFileNamingAndTriggeringPolicy(sizeAndTimeBasedFNATP);
        rollingPolicy.start();
        //必须在rollingPolicy start之后不然会出现FileNamePattern对象为null
        sizeAndTimeBasedFNATP.start();

        appender.setRollingPolicy(rollingPolicy);
        appender.setEncoder(createEncoder());
        appender.start();
        mAppenders.add(appender);
    }

    private void addFileAppender() {
        checkSetPath();
        FileAppender<ILoggingEvent> appender = new FileAppender<ILoggingEvent>();
        appender.setContext(mContext);
        appender.setFile(mLogPath + File.separator + mFilename + ".log");
        appender.setEncoder(createEncoder());
        appender.start();
        mAppenders.add(appender);
    }

    private void addLogAppender() {
        LogcatAppender appender = new LogcatAppender();
        appender.setContext(mContext);
        appender.setEncoder(createEncoder());
        appender.start();
        mAppenders.add(appender);
    }
    //输出格式
    @NotNull
    private PatternLayoutEncoder createEncoder() {
        PatternLayoutEncoder encoder = new PatternLayoutEncoder();
        encoder.setContext(mContext);
        encoder.setPattern(mLogFormat);
        encoder.start();
        return encoder;
    }

    private void addAppender(ch.qos.logback.classic.Logger logger) {
        for (UnsynchronizedAppenderBase<ILoggingEvent> mAppender : mAppenders) {
            logger.addAppender(mAppender);
        }
    }
}